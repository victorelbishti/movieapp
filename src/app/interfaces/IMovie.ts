
export interface IMovie {
    id: number;
    title: string;
    grade: number;
}