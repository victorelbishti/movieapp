import { Component, Input, OnInit } from '@angular/core';
import { MovieService } from 'src/app/services/movie.service';
import { IMovie } from '../../interfaces/IMovie';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss']
})
export class MovieCardComponent implements OnInit {

  constructor(private movieService: MovieService) { }

  @Input()
  movie!: IMovie;

  ngOnInit(): void {
  }

  changeGrade(event: any)
  {
    const { value } = event.target;

    if (value != this.movie?.grade)
    {
      this.movie.grade = value;
      let req = this.movieService.updateMovie(this.movie).subscribe(result => {
        this.movie = result;
      },
      (error) => {
        console.error("Unable to update grade:", error);
      }, 
      () => {
        req.unsubscribe();
      });
    }
  }
}
