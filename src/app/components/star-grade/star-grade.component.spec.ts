import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StarGradeComponent } from './star-grade.component';

describe('StarGradeComponent', () => {
  let component: StarGradeComponent;
  let fixture: ComponentFixture<StarGradeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StarGradeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StarGradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
