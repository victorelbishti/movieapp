import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IMovie } from 'src/app/interfaces/IMovie';
import { MovieService } from 'src/app/services/movie.service';

@Component({
  selector: 'app-movie-page',
  templateUrl: './movie-page.component.html',
  styleUrls: ['./movie-page.component.scss']
})
export class MoviePageComponent implements OnInit {

  movieId: number | null;
  movie!: IMovie;

  constructor(private route: ActivatedRoute, private router: Router, private movieService: MovieService) { 

    const id = this.route.snapshot.paramMap.get('id');

    if (id == null){
      this.router.navigate(['404']);
    }
     
    this.movieId = parseInt(id as string);

    let req = this.movieService.getMovie(this.movieId).subscribe(result => {
      this.movie = result;
    },
    (error) => {
      console.error(error);
    },
    () => {
      req.unsubscribe();
    });

  }

  ngOnInit(): void {
  }

  changeGrade(event: any)
  {
    const { value } = event.target;

    if (value != this.movie?.grade)
    {
      this.movie.grade = value;
      let req = this.movieService.updateMovie(this.movie).subscribe(result => {
        this.movie = result;
      },
      (error) => {
        console.error("Unable to update grade:", error);
      }, 
      () => {
        req.unsubscribe();
      });
    }
  }

}
