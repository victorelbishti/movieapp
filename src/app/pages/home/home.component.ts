import { Component, OnInit } from '@angular/core';
import { IMovie } from 'src/app/interfaces/IMovie';
import { MovieService } from 'src/app/services/movie.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  searchInput: string = "";
  movies: IMovie[] | undefined;

  constructor(private movieService: MovieService){}

  ngOnInit(): void {
    this.movieService.getMovies().subscribe(result => {
      this.movies = result;
    });
  }

}
