import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { IMovie } from 'src/app/interfaces/IMovie';

@Component({
  selector: 'app-star-grade',
  templateUrl: './star-grade.component.html',
  styleUrls: ['./star-grade.component.scss']
})
export class StarGradeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() movie: IMovie | undefined;
  @Output() onGradeChange = new EventEmitter<number>();

  gradeChange(event: any){    
    this.onGradeChange.emit(event);
  }

}
