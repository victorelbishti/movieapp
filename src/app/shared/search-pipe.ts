import { Pipe, PipeTransform } from "@angular/core";
import { IMovie } from "../interfaces/IMovie";

@Pipe({
    name: 'searchFilter'
})
export class SearchPipe implements PipeTransform {
    transform(items: IMovie[], value: string): IMovie[] {
        if (!items || !value || value == ""){
            return items;
        }

        return items.filter(item => {
            if (item.title.toLowerCase().indexOf(value.toLowerCase()) === -1){
                return false;
            }

            return true;
        });
    }
}