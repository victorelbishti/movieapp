import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { IMovie } from "../interfaces/IMovie";

@Injectable({
    providedIn: 'root'
})
export class MovieService {
    private readonly _baseUrl: string;


    constructor(private httpClient: HttpClient){
        this._baseUrl = 'http://localhost:3000';
    }

    getMovies(): Observable<IMovie[]>
    {
        return this.httpClient.get<IMovie[]>(`${this._baseUrl}/videos`);
    }

    updateMovie(movie: IMovie): Observable<IMovie>
    {
        console.log("Update movie");
        console.log(movie);
        return this.httpClient.put<IMovie>(`${this._baseUrl}/videos/${movie.id}`, movie);
    }

    getMovie(id: number): Observable<IMovie>
    {
        return this.httpClient.get<IMovie>(`${this._baseUrl}/videos/${id}`);
    }
}